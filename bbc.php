<?php
    
    $vars = $_POST['link'];
    //echo "<pre>"; print_r($vars); exit;
  
    $url = "http://readability.com/api/content/v1/parser?url=$vars&token=26e018f4faeaf6f4c99699e491597c6b511dd675";
    //echo "<pre>"; print_r($url); exit;
    $ch = curl_init($url);
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
    );
    curl_setopt_array( $ch, $options );
    $result = curl_exec($ch);
    curl_close($ch);
    $result_arr = json_decode($result, true);
    //echo "<pre>"; print_r($result_arr); exit;
?>

    <button class="btn hidden" id="btn-modal" data-toggle="modal" data-target=".bs-example-modal-lg"></button>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?=$result_arr['title']?></h4>
                </div>
            <div class="modal-body"><?=$result_arr['content']?></div>
            </div>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function() {
    $(function(){
       $('#btn-modal').click();
    });
});
</script>

