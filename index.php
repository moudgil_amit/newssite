<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<title>News Site</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="Mebify" />
	<link rel="shortcut icon" href="http://tympanus.net/Development/favicon.ico"> 
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<script src="js/modernizr.custom.js"></script>	


	<style type="text/css">
		.heading {text-align: center; background: white; font-size: 20px; padding-top: 10px; padding-bottom: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; }
		@media (min-width: 480px) {
			#header {float: right; font-family: cursive; font-size: 20px; color: white; margin-top: 1%;}
		}
		@media (max-width: 480px) {
			#header {font-family: cursive; font-size: 18px; color: white; margin-top: 1%;}
			#grid {margin-top: 35%;}
		}
		.card {box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);}
		.heading {box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.22);}
		#space {opacity: 0.9;}
		a:hover, a:focus {outline: none;}
		#bbc-pic {width: 200%; height: 200%;}
		a {cursor: pointer}
	</style>	
</head>

<?php

error_reporting(E_ALL & ~E_NOTICE);

$feeds = array(
    'http://feeds.hindustantimes.com/HT-HomePage-TopStories?format=xml',
    'http://ibnlive.in.com/ibnrss/top.xml',
    'http://feeds.bbci.co.uk/news/rss.xml',
);

    $xml0 = simplexml_load_file($feeds[0]);
    //echo "<pre>"; print_r($xml0);exit;
    $heading0 = array();  
    $link0 = array();
    $image0 = array();  
    foreach($xml0->channel->item as $item)
    {	
    	$heading0[] = $item->title;
    	$link0[] = $item->link;
    	$array1 = json_decode(json_encode($item->enclosure), true);
    	$image0[] = $array1['@attributes']['url'];
		//echo "<pre>"; print_r($image0);exit;
    }
    
    $xml1 = simplexml_load_file($feeds[1]);
    //echo "<pre>"; print_r($xml1);exit;
    $heading1 = array();  
    $link1 = array();
    $image1 = array();  
    foreach($xml1->channel->item as $item)
    {	
    	$heading1[] = $item->title;
    	$link1[] = $item->link;
    	$a = $item->description;
    	$b = explode("<", $a);
    	$c = implode("", $b);  	
    	$d = explode(" ", $c);
    	$e = explode("=", $d[1]);
    	$f = explode('"', $e[1]);
    	$image1[] = $f[1];	 
    }
     
    $xml2 = simplexml_load_file($feeds[2]);
    //echo "<pre>"; print_r($xml0);exit;
    $heading2 = array();  
    $link2 = array();
    $image2 = array();  
    $namespace = "http://search.yahoo.com/mrss/";
    foreach($xml2->channel->item as $item)
    {	
    	$heading2[] = $item->title;
    	//echo $heading; exit;
    	$link2[] = $item->link;
    	$image = $item->children($namespace)->thumbnail[1]->attributes();
    	$image2[] = $image['url'];
    	//echo "<pre>"; print_r($image2);exit;	 
    }

?>

	<body>
		<div class="navbar navbar-inverse navbar-fixed-top" id="space" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand" style="color:white; font-size:30px; font-family:cursive;">News-Site</a>
                </div>
                <p id="header"> Find latest Hindustan Times / IBN Live / BBC news content here </p>
            </div>               
        </div>   

		<div class="container" style="margin-top:7%;">

			<div id="newscontent"></div>

			<ul class="grid effect-2" id="grid">
				<?php for($i=0;$i<=21;$i++) {

					echo '<li><a class="card" onclick="getLink0(\''.$link0[$i][0].'\');"'; echo 'id="ht-news"><img src="'; echo $image0[$i]; echo '"></a>
					<div class="heading">'; echo $heading0["$i"]["0"];'</div></li>';

					echo '<li><a class="card" onclick="getLink1(\''.$link1[$i][0].'\');"'; echo 'id="ibn-news"><img src="'; echo $image1[$i]; echo '"></a>
					<div class="heading">'; echo $heading1["$i"]["0"];'</div></li>';

					echo '<li><a class="card" onclick="getLink2(\''.$link2[$i][0].'\');"'; echo 'id="bbc-news"><img id="bbc-pic" src="'; echo $image2[$i]; echo '"></a>
					<div class="heading">'; echo $heading2["$i"]["0"];'</div></li>';
				}
			echo'</ul>';?>
		</div>

		<script src="js/masonry.pkgd.min.js"></script>
		<script src="js/imagesloaded.js"></script>
		<script src="js/classie.js"></script>
		<script src="js/AnimOnScroll.js"></script>
		<script>
			new AnimOnScroll( document.getElementById("grid"), {
				minDuration : 0.4,
				maxDuration : 0.7,
				viewportFactor : 0.2
			} );
		</script>
		<script src="js/jquery-1.11.0.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			function getLink0(link)
			{
				$.ajax({
	                type: "post",
	                url: "ht.php",
	                data: {'link':link},                   
	                success: function(data){
	                	$("#newscontent").html("");
			            $("#newscontent").html(data);
			        },
	                error: function(){
	                    console.log("error");
	                }
	            });	
			}
			function getLink1(link)
			{
				$.ajax({
	                type: "post",
	                url: "ibn.php",
	                data: {'link':link},                   
	                success: function(data){
	                	$("#newscontent").html("");
			            $("#newscontent").html(data);
			        },
	                error: function(){
	                    console.log("error");
	                }
	            });	
			}
			function getLink2(link)
			{
				$.ajax({
	                type: "post",
	                url: "bbc.php",
	                data: {'link':link},                   
	                success: function(data){
	                	$("#newscontent").html("");
			            $("#newscontent").html(data);
			        },
	                error: function(){
	                    console.log("error");
	                }
	            });	
			}
		</script>	
	</body>
</html>